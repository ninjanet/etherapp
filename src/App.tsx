import React from 'react';
import AppState from './context/background/AppState';
import { Routes } from './routes';

const App = () => {
  return (
    <AppState>
      <Routes />
    </AppState>
  );
};

export default App;
