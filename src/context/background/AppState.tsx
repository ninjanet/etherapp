import React, { useReducer } from 'react';
import AppContext, { initAppState } from './AppContext';
import AppReducer from './AppReducer';
import Actions from '../contextActions';
import { Transaction, IAppState, AccountApiInformation } from '../../types';

const AppState = (props: any) => {
  const [state, dispatch] = useReducer(AppReducer, initAppState);

  // Set app state
  const setState = (newState: IAppState) => {
    dispatch({
      type: Actions.SET_STATE,
      payload: newState,
    });
  };

  const addTransaction = (transaction: Transaction) => {
    dispatch({ type: Actions.ADD_TRANSACTION, payload: transaction });
  };

  const setAccountInformation = (accountInformation: AccountApiInformation) => {
    dispatch({
      type: Actions.SET_ACCOUNT_INFORMATION,
      payload: accountInformation,
    });
  };

  const setTransactions = (transactions: Array<Transaction>) => {
    dispatch({
      type: Actions.SET_TRANSACTIONS,
      payload: transactions,
    });
  };

  return (
    <AppContext.Provider
      value={{
        state,
        setState,
        addTransaction,
        setAccountInformation,
        setTransactions,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppState;
