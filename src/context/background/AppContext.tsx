import React, { createContext } from 'react';
import { Transaction, IAppState, AccountApiInformation } from '../../types';

export const initAppState: IAppState = {
  transactions: [],
  accountInformation: {
    publicAddress: '',
  },
};

type ContextType = {
  state: IAppState;
  addTransaction: (transaction: Transaction) => void;
  setState: (state: IAppState) => void;
  setAccountInformation: (accountInformation: AccountApiInformation) => void;
  setTransactions: (transactions: Array<Transaction>) => void;
};

const AppContext: React.Context<ContextType> = createContext<ContextType>({
  state: initAppState,
  addTransaction: () => {},
  setState: () => {},
  setAccountInformation: () => {},
  setTransactions: () => {},
});

export default AppContext;
