const font = {
  family: {
    averta: 'Averta, serif',
    inter: 'Inter, sans-serif',
  },
  weight: {
    thin: 100,
    extraLight: 200,
    light: 300,
    regular: 400,
    medium: 500,
    semiBold: 600,
    bold: 700,
    extraBold: 800,
    black: 900,
  },
};

// Fonts Loaded
//
// Averta
// 100: Extrathin + italic
// 200: Thin + italic
// 300: Light + italic
// 400: Regular + italic
// 500: ---
// 600: Semibold + italic
// 700: Bold + italic
// 800: Extrabold + italic
// 900: Black + italic
//
// Inter
// 100: thin + italic
// 200: ExtraLight + Italic
// 300: Light + italic
// 400: Regular + italic
// 500: Medium + italic
// 600: Semibold + italic
// 700: Bold + Italic
// 800: Extrabold + italic
// 900: Black + italic

export default font;
