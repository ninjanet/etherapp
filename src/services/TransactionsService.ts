import { BaseService } from './infrastructure/BaseService';
import {
  TransactionsServiceState,
  Transaction,
  AccountApiInformation,
} from '../types';
import apiAccountResponse from '../utils/constants';
import {
  transformApiTransactions,
  transformApiAccountInformation,
} from '../utils/transform';

/**
 * The transaction object
 * Value is of type `number` for simplification
 */

/**
 * TransacionsService class
 * TODO: Complete the addTransaction and the getListOfTransactions methods
 */
export class TransactionsService extends BaseService<TransactionsServiceState> {
  constructor(initialState: TransactionsServiceState) {
    super(
      initialState || {
        transactions: [],
      },
    );
  }

  /**
   * It adds a transaction to the list
   * TODO: Complete addTransaction code inside the Promise resolve function
   */
  public async addTransaction(
    newTransaction: Transaction,
  ): Promise<Transaction> {
    return new Promise<Transaction>((resolve) => {
      setTimeout(() => {
        resolve(newTransaction);
      }, 300);
    });
  }

  /**
   * It returns the list of transactions
   * TODO: Return the list via the promise resolve function
   */
  public async getListOfTransactions(): Promise<Array<Transaction>> {
    return new Promise<Array<Transaction>>((resolve) => {
      setTimeout(() => {
        resolve(transformApiTransactions(apiAccountResponse));
      }, 300);
    });
  }

  public async getAccountInformation(): Promise<AccountApiInformation> {
    return new Promise<AccountApiInformation>((resolve) => {
      setTimeout(() => {
        resolve(transformApiAccountInformation(apiAccountResponse));
      }, 300);
    });
  }
}
