import { AccountApiResponsePayload } from '../types';

const defaults: AccountApiResponsePayload = {
  publicAddress: '0xb701FdCc9Db05d5AD0d7B6aAbb42DBf09ec28Ad0',
  accountBalance: 3.405,
  ethPrice: 1700,
  pastTransactions: {
    0: {
      date: '06-01-2021 19:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    1: {
      date: '06-01-2021 20:00',
      amount: '2',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    2: {
      date: '06-01-2021 21:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
  },
};

const largeSet: AccountApiResponsePayload = {
  publicAddress: '0xb701FdCc9Db05d5AD0d7B6aAbb42DBf09ec28Ad0',
  accountBalance: 3.405,
  ethPrice: 1700,
  pastTransactions: {
    0: {
      date: '06-01-2021 19:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    1: {
      date: '06-01-2021 20:00',
      amount: '2',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    2: {
      date: '06-01-2021 21:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    3: {
      date: '06-01-2021 19:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    4: {
      date: '06-01-2021 20:00',
      amount: '2',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    5: {
      date: '06-01-2021 21:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    6: {
      date: '06-01-2021 19:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    7: {
      date: '06-01-2021 20:00',
      amount: '2',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    8: {
      date: '06-01-2021 21:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    9: {
      date: '06-01-2021 19:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    10: {
      date: '06-01-2021 20:00',
      amount: '2',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
    11: {
      date: '06-01-2021 21:00',
      amount: '3',
      recipient: '0xb19181c403D451A1e161b305eb08DfD422ffd6DD',
    },
  },
};
export default defaults;
