import Actions, {
  SetStateAction,
  SetTransactionAction,
  SetAccountInformationAction,
  AddTransactionAction,
} from '../contextActions';
import { IAppState } from '../../types';

type AppAction =
  | SetTransactionAction
  | SetStateAction
  | SetAccountInformationAction
  | AddTransactionAction;

const appReducer = (state: IAppState, action: AppAction): IAppState => {
  switch (action.type) {
    case Actions.SET_STATE:
      return {
        ...state,
        ...action.payload,
      };
    case Actions.ADD_TRANSACTION:
      return {
        ...state,
        transactions: [...state.transactions, ...[action.payload]],
      };
    case Actions.SET_TRANSACTIONS: {
      const transactionIds = state.transactions.map((t) => t.id);
      const uniqueTransactions = action.payload.filter(
        (transaction) => !transactionIds.includes(transaction.id),
      );
      return {
        ...state,
        transactions: [...state.transactions, ...uniqueTransactions],
      };
    }
    case Actions.SET_ACCOUNT_INFORMATION: {
      return {
        ...state,
        accountInformation: {
          ...state.accountInformation,
          ...action.payload,
        },
      };
    }
    default:
      return state;
  }
};

export default appReducer;
