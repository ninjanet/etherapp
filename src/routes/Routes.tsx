import React from 'react';

import { Route, Switch } from 'react-router-dom';

import { Dashboard, Send, Success } from '../screens';
import Router from './Router';

const Routes: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path={'/'} exact>
          <Dashboard />
        </Route>
        <Route path={'/send'} exact>
          <Send />
        </Route>
        <Route path={'/success'} exact>
          <Success />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
