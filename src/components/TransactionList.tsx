import React, { useContext } from 'react';

import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core';
import dayjs from 'dayjs';

import AppContext from '../context/background/AppContext';
import { ethToUsd } from '../utils/transform';
import CustomSvgIcons from './CustomSvgIcons';
import { Transaction } from '../types';

const TransactionList: React.FC = () => {
  const { state } = useContext(AppContext);
  const { transactions } = state;
  return (
    <List>
      {transactions.map((transaction: Transaction, index: number) => (
        <ListItem key={`${index}-transaction`}>
          <ListItemAvatar>
            <CustomSvgIcons.EtherIcon />
          </ListItemAvatar>
          <ListItemText
            primary="Sent Ether"
            secondary={dayjs(transaction.date).format('MM-DD-YYYY')}
          />
          <ListItemText
            primary={`-${transaction.value} ETH`}
            secondary={`-${ethToUsd(transaction.value)}`}
          />
        </ListItem>
      ))}
    </List>
  );
};

export default TransactionList;
