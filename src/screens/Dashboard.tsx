import React, { useEffect, useContext } from 'react';

import { useHistory } from 'react-router-dom';
import { Divider } from '@material-ui/core';

import { Header, SendButton, TransactionList } from '../components';

import { TransactionsService } from '../services/TransactionsService';
import AppContext from '../context/background/AppContext';

const Dashboard: React.FC = () => {
  const history = useHistory();
  const { state, setAccountInformation, setTransactions } = useContext(
    AppContext,
  );

  useEffect(() => {
    const transactionService = new TransactionsService(state);
    const getAccountInformation = async () => {
      const accountInformation = await transactionService.getAccountInformation();
      setAccountInformation(accountInformation);
    };
    getAccountInformation();
  }, [setAccountInformation, state]);

  useEffect(() => {
    const transactionService = new TransactionsService(state);
    const getTransactions = async () => {
      const transactions = await transactionService.getListOfTransactions();
      setTransactions(transactions);
    };
    getTransactions();
  }, [setTransactions, state]);

  return (
    <>
      <Header
        title={'Account 1'}
        subtitle={state.accountInformation.publicAddress}
        avatar={'G'}
      />
      <Divider></Divider>
      <SendButton
        onClick={() => {
          history.push('/send');
        }}
      ></SendButton>
      <TransactionList />
    </>
  );
};

export default Dashboard;
