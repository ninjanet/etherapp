const colors = {
  background: {
    accordion: '#F4F5F6',
    dark: '#3D4455',
    light: '#ffffff',
    medium: '#FAFAFA',
  },
  blue: '#2E4474',
  bluegray: {
    100: '#535C70',
    88: '#687081',
    77: '#7B8291',
    66: '#8E93A1',
    44: '#B3B7C0',
    33: '#C6C9D0',
    22: '#D9DBE0',
    11: '#ECEDEF',
  },
  border: {
    light: '#F4F5F6',
    medium: '#ECECED',
  },
  brand: {
    teal: '#1FEBC3',
  },
  error: '#FA4747',
  text: {
    100: '#252727',
    88: '#3F4141',
    66: '#6F7171',
    55: '#878888',
    44: '#9FA0A0',
    33: '#B7B8B8',
    22: '#CFCFCF',
    11: '#E7E7E7',
  },
  hover: '#F3F3F3',
  input: {
    bg: {
      default: '#ffffff',
      disabled: '#dedede',
      focus: '#ffffff',
      hover: '#FAFBFC',
    },
    border: {
      default: '#ECECED',
      disabled: '#dedede',
      focus: '#99a3c4',
      hover: '#dfdfe0',
    },
  },
  placeholder: '#9FA0A0',
  primary: {
    100: '#6031E8',
    88: '#734AEB',
  },
  secondary: {
    100: '#F5F6F7',
    88: '#F6F7F8',
  },
  tertiary: {
    100: '#11D4A0',
    88: '#2ED9AB',
  },
  selected: {
    dark: '#E6E8EA',
    light: 'rgba(37,39,39,.055)',
  },
  status: {
    actionable: {
      bg: '#F2EFFD',
      fg: '#6031E8',
    },
    canceled: {
      bg: '#ffffff',
      fg: '#252727',
    },
    caution: {
      bg: '#FFF7E5',
      fg: '#FFD77D',
    },
    error: {
      bg: '#FDECEC',
      fg: '#F48282',
    },
    neutral: {
      bg: '#EEEFF1',
      fg: '#535C70',
    },
    success: {
      bg: '#D7F8EF',
      fg: '#11D4A0',
    },
  },
  table: {
    header: '#F5F6F7',
  },
  white: '#ffffff',
};

export default colors;
