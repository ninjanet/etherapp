import React from 'react';

import { useHistory } from 'react-router-dom';

import {
  Box,
  Typography,
  CardContent,
  Link,
  CardActions,
  Button,
} from '@material-ui/core';
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import styled from 'styled-components';

import { StyledCard } from './styles';

const XLargeSuccessIcon = styled(CheckCircleOutline)`
  && {
    font-size: 10rem;
  }
`;

const StyledLink = styled(Link)`
  && {
    vertical-align: middle;
    display: inline-flex;
  }
`;

const CenteredCardActions = styled(CardActions)`
  margin-top: 50px;
  justify-content: center;
`;
const Success: React.FC = () => {
  const history = useHistory();

  return (
    <StyledCard>
      <CardContent>
        <Box
          display="flex"
          justifyContent="center"
          m={2}
          p={2}
          flexDirection="column"
        >
          <Typography gutterBottom variant="h1" component="h1" align="center">
            <XLargeSuccessIcon color="secondary" />
          </Typography>
        </Box>
        <Typography gutterBottom variant="h5" component="h2" align="center">
          Success
        </Typography>
        <Typography
          variant="body2"
          color="textSecondary"
          component="p"
          align="center"
        >
          You've successfully sent your funds
          <br></br>
          <StyledLink href="https://etherscan.io/" target="blank">
            <span>View on Etherscan </span>
            <ArrowRightAltIcon />
          </StyledLink>
        </Typography>
      </CardContent>
      <CenteredCardActions>
        <Button
          color="primary"
          variant="contained"
          onClick={() => history.push('/')}
        >
          Done
        </Button>
      </CenteredCardActions>
    </StyledCard>
  );
};

export default Success;
