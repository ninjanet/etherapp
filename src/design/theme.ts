import { breakpoints, colors, font, shadow, spacing } from './tokens';

const theme = {
  borderRadius: {
    small: 11,
    medium: 16.5,
    large: 22,
  },
  borderWidth: 1,
  breakpoint: {
    ...breakpoints,
  },
  button: {
    height: {
      large: 44,
      small: 33,
    },
  },
  card: {
    minHeight: {
      large: 110,
      small: 66,
    },
  },
  color: {
    ...colors,
  },
  dropdown: {
    item: {
      height: {
        desktop: 33,
        mobile: 44,
      },
    },
  },
  font: {
    ...font,
  },
  header: {
    height: 66,
  },
  input: {
    height: 44,
  },
  layout: {
    gutter: {
      large: 55,
      small: 22,
    },
  },
  modal: {
    footer: {
      // (spacing x2) + buttonHeight
      height: 132,
    },
    spacing: {
      large: 44,
      small: 33,
    },
  },
  nav: {
    paddingH: 11,
    width: {
      closed: 66,
      open: 244,
    },
  },
  page: {
    header: {
      padding: {
        large: 44,
        small: 33,
      },
    },
  },
  rem: 16,
  shadow: { ...shadow },
  sidebar: {
    footer: {
      height: {
        large: 154,
        small: 88,
      },
    },
    padding: 33,
    width: {
      xlarge: 462,
      large: 400,
      small: '100%',
    },
  },
  spacing: {
    ...spacing,
  },
  table: {
    cell: {
      height: 66,
    },
    label: {
      offset: 19,
    },
  },
  toast: {
    timing: 5000,
  },
  transition: {
    button: {
      default: 'ease-out 100ms',
      fast: 'ease-out 75ms',
    },
    icon: '100ms ease-in',
    nav: '225ms ease-out',
    spring: {
      accordion: {
        type: 'spring',
        damping: 40,
        stiffness: 500,
      },
    },
  },
  user: {
    item: {
      height: 66,
    },
  },
  zIndex: {
    content: 0,
    contentHeader: 100,
    dialog: 700,
    filter: 200,
    header: 600,
    modal: 800,
    nav: 500,
    overlay: 300,
    sidebar: 400,
    toast: 900,
  },
};

export default theme;
