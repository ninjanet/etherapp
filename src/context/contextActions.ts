import { Transaction, IAppState, AccountApiInformation } from '../types';

enum Actions {
  SET_TRANSACTIONS = 'SET_TRANSACTIONS',
  SET_STATE = 'SET_STATE',
  SET_ACCOUNT_INFORMATION = 'SET_ACCOUNT_INFORMATION',
  ADD_TRANSACTION = 'ADD_TRANSACTION',
}

export interface SetTransactionAction {
  type: Actions.SET_TRANSACTIONS;
  payload: Transaction[];
}

export interface AddTransactionAction {
  type: Actions.ADD_TRANSACTION;
  payload: Transaction;
}

export interface SetAccountInformationAction {
  type: Actions.SET_ACCOUNT_INFORMATION;
  payload: AccountApiInformation;
}

export interface SetStateAction {
  type: Actions.SET_STATE;
  payload: Partial<IAppState>;
}

export default Actions;
