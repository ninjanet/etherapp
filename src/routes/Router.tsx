import { createBrowserHistory } from 'history';
import { BrowserRouter } from 'react-router-dom';

export const browserHistory = createBrowserHistory();

class Router extends BrowserRouter {
  history = browserHistory;
}

export default Router;
