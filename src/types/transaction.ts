import { Dayjs } from 'dayjs';

export interface Transaction {
  id: number;
  to: string;
  from: string;
  value: number;
  date: Dayjs;
}

export interface TransactionsServiceState {
  transactions: Array<Transaction>;
}

export interface AccountApiInformation {
  publicAddress: string;
  accountBalance?: number;
  ethPrice?: number;
}

export interface AccountApiResponsePayload extends AccountApiInformation {
  pastTransactions: Record<string, TransactionApi>;
}

export interface TransactionApi {
  date: string;
  amount: string;
  recipient: string;
}
