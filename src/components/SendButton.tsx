import React, { useContext } from 'react';

import styled from 'styled-components';

import CardContent from '@material-ui/core/CardContent';
import ArrowUpward from '@material-ui/icons/ArrowUpwardRounded';
import { Button, Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';

import AppContext from '../context/background/AppContext';
import { ethToUsd } from '../utils/transform';

const XLargeArrowIcon = styled(ArrowUpward)`
  font-size: 4.5rem;
`;

const StyledButton = styled(Button)`
  && {
    text-transform: none;
  }
`;

interface SendButtonProps {
  onClick: () => void;
}

const SendButton: React.FC<SendButtonProps> = ({
  onClick,
}: SendButtonProps) => {
  const { state } = useContext(AppContext);
  const { accountBalance } = state.accountInformation;
  return (
    <>
      <StyledButton onClick={onClick} fullWidth>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" align="center">
            {`${accountBalance || ''} ETH`}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            align="center"
          >
            {ethToUsd(accountBalance)}
          </Typography>
          <Box
            display="flex"
            justifyContent="center"
            m={2}
            p={2}
            flexDirection="column"
          >
            <Typography gutterBottom variant="h3" component="h3" align="center">
              <XLargeArrowIcon color="primary" fontSize="inherit" />
              <Typography align="center">Send</Typography>
            </Typography>
          </Box>
        </CardContent>
      </StyledButton>
    </>
  );
};

export default SendButton;
