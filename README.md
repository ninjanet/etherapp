# Blank Wallet Code Challenge

React app with typescript template

# Steps to run the app

    - yarn
    - yarn start

## Task

Build an app to send ether with the given user interface.

    - Transactions must be stored and fetched making use of the TransactionsService addTransaction and getListOfTransactions methods - Done
    - Past transactions must be listed on the main page - Done
    - You would need to add an "amount to transfer" input field (not present in the image) - Done
    - Styling and layout do not have to be necessarily like the provided image - Done

## What will be evaluated?

    - Interface and "pixel perfect" design will not be taken into account.
    - Responsive design
    - Architecture skills
    - React skills

## Layout

(Layout.gif)
