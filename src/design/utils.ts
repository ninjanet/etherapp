import theme from './theme';
import { ScaleTypes } from './types/ScaleTypes';

export const getScale: (multiplyer: ScaleTypes) => number = (
  multiplyer: ScaleTypes,
) => {
  const BASESIZE = 11;
  return BASESIZE * multiplyer;
};

export const toRem: (n: number) => string = (n: number) =>
  `${n / theme.rem}rem`;
