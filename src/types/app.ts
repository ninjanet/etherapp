import { Transaction, AccountApiInformation } from './transaction';

export interface IAppState {
  transactions: Array<Transaction>;
  accountInformation: AccountApiInformation;
}
