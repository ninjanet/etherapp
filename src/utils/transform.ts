import dayjs from 'dayjs';
import {
  Transaction,
  AccountApiResponsePayload,
  TransactionApi,
  AccountApiInformation,
} from '../types';

const usdDefaultEthValue: string = ' 2362,92';

export const transformApiTransactions = (
  apiAccountInfo: AccountApiResponsePayload,
): Array<Transaction> => {
  return Object.values(apiAccountInfo?.pastTransactions).map(
    (transactionApi: TransactionApi, index: number) => ({
      id: index,
      from: apiAccountInfo.publicAddress,
      to: transactionApi.recipient,
      value: parseInt(transactionApi.amount),
      date: dayjs(transactionApi.date),
    }),
  );
};

export const transformApiAccountInformation = (
  apiAccountInfo: AccountApiResponsePayload,
): AccountApiInformation => ({
  publicAddress: apiAccountInfo.publicAddress,
  accountBalance: apiAccountInfo.accountBalance,
  ethPrice: apiAccountInfo.ethPrice,
});

export const ethToUsd = (eth?: number): string =>
  eth ? `${eth * parseFloat(usdDefaultEthValue)} USD` : '';
