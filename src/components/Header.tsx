import React from 'react';

import styled from 'styled-components';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';

const HeaderLayout = styled(Card)`
  max-width: 345;
  overflow-wrap: break-word;
  overflow: hidden;
  .MuiCardHeader-content {
    overflow: hidden;
    word-wrap: break-word;
  }
`;

interface HeaderProps {
  avatar?: string;
  title?: string;
  subtitle?: string;
  action?: React.ReactNode;
}

const Header: React.FC<HeaderProps> = ({
  avatar,
  title,
  subtitle,
  action,
}: HeaderProps) => {
  return (
    <HeaderLayout>
      <CardHeader
        avatar={avatar && <Avatar aria-label="recipe">G</Avatar>}
        title={title}
        subheader={subtitle}
        action={action}
      />
    </HeaderLayout>
  );
};

export default Header;
