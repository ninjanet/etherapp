import React, { useContext, useReducer } from 'react';

import { useHistory } from 'react-router-dom';
import {
  Button,
  CardActions,
  Grid,
  IconButton,
  TextField,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import styled from 'styled-components';
import { useFormik } from 'formik';
import * as yup from 'yup';
import dayjs from 'dayjs';

import { Header } from '../components';

import breakpoints from '../utils/breakpoints';
import { TransactionsService } from '../services/TransactionsService';
import AppContext from '../context/background/AppContext';
import AppReducer from '../context/background/AppReducer';
import { Transaction } from '../types';
import { StyledCard } from './styles';

interface SendFormProps {
  recipient: string;
  amount: number;
}

const StyledCardContent = styled.div`
  && {
    margin-bottom: 20px;
    padding: 20px;
  }
`;

const StyledTextInput = styled(TextField)`
  && {
    margin-top: 30px;
  }
`;

const MobileSendForm = styled.form`
  && {
    justify-content: space-between;
    flex-direction: column;
    flex: 1 1 auto;
  }
  @media (min-width: ${breakpoints.large}px) {
    display: none;
  }
  @media (max-width: ${breakpoints.large}px) {
    display: flex;
  }
`;

const SendForm = styled.form`
  @media (max-width: ${breakpoints.large}px) {
    display: none;
  }
`;

const validationSchema = yup.object({
  recipient: yup.string().required('Recipient address is Required'),
  amount: yup
    .number()
    .moreThan(0, 'Etherium amount needs to be more than 0')
    .required('Etherium amount is required'),
});

const Send: React.FC = () => {
  const history = useHistory();
  const {
    state: contextState,
    addTransaction: addTransactionState,
  } = useContext(AppContext);
  const [state] = useReducer(AppReducer, contextState);

  const formik = useFormik({
    initialValues: {
      recipient: '',
      amount: 0,
    },
    validationSchema: validationSchema,
    onSubmit: (values: SendFormProps) => {
      const addTransaction = async () => {
        const transactionService = new TransactionsService(state);

        const payload: Transaction = {
          id: state.transactions.length + 1,
          to: values.recipient,
          from: state.accountInformation.publicAddress,
          value: values.amount,
          date: dayjs(),
        };
        const transaction = await transactionService.addTransaction(payload);
        addTransactionState(transaction);
        history.push('/success');
      };
      addTransaction();
    },
  });

  const cancel = () => history.push('/');
  const validateField = (field: 'recipient' | 'amount') =>
    Boolean(formik.touched[field] && formik.errors[field]);
  const inputRecipientComponent = (
    <StyledTextInput
      autoComplete="off"
      id="recipient"
      label="Add Recipient"
      fullWidth
      placeholder="Enter Public Address"
      error={validateField('recipient')}
      helperText={validateField('recipient') ? formik.errors.recipient : ''}
      {...formik.getFieldProps('recipient')}
    />
  );
  const inputAmountComponent = (
    <StyledTextInput
      autoComplete="off"
      type="number"
      id="amount"
      label="Add mount"
      fullWidth
      placeholder="Enter Etherium amount"
      error={validateField('amount')}
      helperText={validateField('amount') ? formik.errors.amount : ''}
      {...formik.getFieldProps('amount')}
    />
  );

  return (
    <StyledCard>
      <Header
        title={'Send Ether'}
        action={
          <IconButton aria-label="close" color="inherit" onClick={cancel}>
            <CloseIcon />
          </IconButton>
        }
      />
      <MobileSendForm onSubmit={formik.handleSubmit}>
        <StyledCardContent>
          {inputRecipientComponent}
          {inputAmountComponent}
        </StyledCardContent>
        <CardActions>
          <Button
            color="primary"
            variant="contained"
            fullWidth
            onClick={cancel}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            fullWidth
            type="submit"
            disabled={!(formik.isValid && formik.dirty)}
          >
            Submit
          </Button>
        </CardActions>
      </MobileSendForm>
      <SendForm onSubmit={formik.handleSubmit}>
        <StyledCardContent>
          <Grid item sm={6}>
            {inputRecipientComponent}
          </Grid>
          <Grid item sm={6}>
            {inputAmountComponent}
          </Grid>
        </StyledCardContent>
        <CardActions>
          <Button color="primary" variant="contained" onClick={cancel}>
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            type="submit"
            disabled={!(formik.isValid && formik.dirty)}
          >
            Submit
          </Button>
        </CardActions>
      </SendForm>
    </StyledCard>
  );
};

export default Send;
